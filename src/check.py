# -*- coding: utf8 -*-
import dbmodel
import numpy as np
import matplotlib.pyplot as plt


def averf():
    result = []
    for i in range(200):
        print i
        result.append(np.loadtxt("./f/%d.txt" % i))
    f = np.asarray(result)
    np.savetxt("./f/mean.txt", f.mean(axis=0))
    np.savetxt("./f/var.txt", f.var(axis=0))


def boundry():
    s = dbmodel.SqlSession("sqlite:///te.db")
    session = s.createASession()
    #q = session.query(dbmodel.Data).filter(dbmodel.Data.timesofrun==0).\
        #filter(dbmodel.Data.step==0).all()
    #r = boundry(session)
    mylist = [0] * 100
    result = []
    for i in range(100):
        # generations
        cum = 0
        for j in range(100):
        # times of run
            q = session.query(dbmodel.Data).filter(dbmodel.Data.timesofrun==j).\
                filter(dbmodel.Data.step==i).all()
            for item in q:
                mylist[item.index] = item.allele
            for index in range(99):
                if mylist[index] != mylist[index + 1]:
                    cum += 1
        result.append(cum / 100.)
        print i, cum / 100.
    return result


def h():
    point = [0, 64, 500, 1024, 2048, 4096, 9000]
    #point = [2048]
    for i in point:
        print i
        b = np.zeros(500)
        for j in range(500):
            b += np.loadtxt("./data/%d.%d.txt" % (j, i))[:, 1]
        b = b / 500.
        with open("./newdata/%d.txt" % i, "w") as f:
            np.savetxt(f, b)

def averageHeter():
    point = [0, 64, 500, 1024, 2048, 4096, 9000]
    for i in point:
        a = np.loadtxt("./newdata/0/%d.txt" % i)
        plt.scatter(np.arange(500), a)
    plt.show()

if __name__ == "__main__":
    averf()
