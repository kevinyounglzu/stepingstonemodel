# -*- coding: utf8 -*-
import random
import matplotlib.pyplot as plt
import dbmodel
import numpy as np


class StepingStone(object):
    def __init__(self, L, s, mu12, mu21):
        """
        @param L: the length of the wanted list
        @param s: the selection advantage of allele one. s is way less than 1.
        @param mu12: mutation rate from allele one to two
        @param mu21: mutation rate from allele two to one
        """
        self.L = L
        self.s = s
        self.mu12 = mu12
        self.mu21 = mu21
        self.generation = 0

        self.transition_prob = self._getTransitionProb()
        self.the_list, self.temp_list = self._initializeConfig()

    def _initializeConfig(self):
        """
        Initialize two list.
        @return the_list, temp_list: tow list with length L. the_list is random, temp_list is zero
        """
        the_list = []
        temp_list = []
        for i in range(self.L):
            the_list.append(random.randint(1, 2))
            temp_list.append(0)

        return the_list, temp_list

    def _getTransitionProb(self):
        """
        Get the transition probability.
        @return transition_prob: wanted transition probability
        """
        transition_prob = dict()
        transition_prob[2] = 1 - self.mu12
        transition_prob[3] = (0.5 + self.s / 4.) * (1 - self.mu12) + (0.5 - self.s / 4.) * self.mu21
        transition_prob[4] = self.mu21
        return transition_prob

    def oneGeneration(self):
        """
        Main logic of the model.
        """
        remainder = self.generation % 2

        for i in range(self.L):
            one = i - 1 + remainder
            two = i + remainder

            if one < 0:
                one = self.L - 1
            if two >= self.L:
                two = 0

            tranp = self.transition_prob[self.the_list[one] + self.the_list[two]]

            if random.random() < tranp:
                self.temp_list[i] = 1
            else:
                self.temp_list[i] = 2

        self.the_list, self.temp_list = self.temp_list, self.the_list
        self.generation += 1


def plot():
    step = 101
    L = 100
    s = 0.1
    mu21 = 0
    mu12 = 0

    ss = StepingStone(L, s, mu12, mu21)

    xs = []
    ys = []
    for i in range(step):
        remainder = ss.generation % 2
        for index, item in enumerate(ss.the_list):
            if item == 1:
                xs.append(index - remainder / 2.)
                ys.append(ss.generation)
        ss.oneGeneration()

    plt.hexbin(xs, ys,  gridsize=(L, (step - 1) / 2), edgecolors="none")
    plt.show()


class InterwithDb(object):
    def __init__(self, db_name, stepingstone, timesofrun):
        sqlsession = dbmodel.SqlSession(db_name)
        sqlsession.initializeDb()
        self.session = sqlsession.createASession()
        self.stepingstone = stepingstone
        self.timesofrun = timesofrun

    def dumpSetting(self):
        self.session.add(
            dbmodel.Setting(
                length=self.stepingstone.L,
                s=self.stepingstone.s,
                mu12=self.stepingstone.mu12,
                mu21=self.stepingstone.mu21
            )
        )
        self.session.commit()

    def dumpConfigeration(self):
        for i, allele in enumerate(self.stepingstone.the_list):
            self.session.add(
                dbmodel.Data(
                    generation=self.stepingstone.generation,
                    index=i,
                    allele=allele,
                    timesofrun=self.timesofrun
                )
            )
        self.session.commit()


def getheter():
    point = [0, 64, 500, 1024, 2048, 4096, 9000]
    for i in range(100):
        print i
        heter(i, point)


def heter(timesofrun, point):
    step = 10000
    L = 1000
    s = 0.
    mu21 = 0
    mu12 = 0

    ss = StepingStone(L, s, mu12, mu21)

    for i in range(step):
        if i in point:
            file_name = "./data/%d.%d.txt" % (timesofrun, i)
            np.savetxt(file_name, getHeterozygosity(ss))
        ss.oneGeneration()


def getHeterozygosity(stepingstone):
    """
    Return the heterozygosity of the system.
    """
    h = []
    for intervel in range(stepingstone.L / 2):
        cum = 0
        for index in range(stepingstone.L):
            nindex = index + intervel
            if nindex > stepingstone.L - 1:
                nindex = nindex - stepingstone.L
            cum += 0 if stepingstone.the_list[index] == stepingstone.the_list[nindex] else 1
        h.append((intervel, cum / float(stepingstone.L)))
    return h


def getf(timesofrun):
    L = 1000
    s = 0.
    mu21 = 0
    mu12 = 0
    #session = None
    ss = StepingStone(L, s, mu12, mu21)
    step = int(1e6)
    file_name = "./f/%d.txt" % timesofrun
    with open(file_name, "a") as f:
        for i in range(step):
            f.write("%f\n" % (getTotalF(ss)))
            #print i, getTotalF(ss)
            ss.oneGeneration()


def getTotalF(stepingstone):
    """
    Return the average frequency of one allel.
    """
    return (2 * stepingstone.L - sum(stepingstone.the_list)) / float(stepingstone.L)


def boundry(s):
    L = 100
    #s = 0.1
    mu21 = 0
    mu12 = 0

    models = []
    for i in range(100):
        models.append(StepingStone(L, s, mu12, mu21))

    result = []
    for i in range(100):
        bs = []
        for model in models:
            bs.append(getBoundry(model))
            model.oneGeneration()
        result.append(np.mean(bs))
    return result


def getBoundry(stepingstone):
    cum = 0
    for i in range(stepingstone.L - 1):
        if stepingstone.the_list[i] != stepingstone.the_list[i + 1]:
            cum += 1
    return cum


def final(timesofrun):
    L = 3200
    step = int(4e5)
    ss = StepingStone(L, 0, 0, 0)
    for i in range(step):
        #print i
        ss.oneGeneration()
    np.savetxt("./h/%d.txt" % timesofrun, getHeterozygosity(ss))


def run():
    #step = 101
    L = 100
    s = 0.1
    mu21 = 0
    mu12 = 0
    db_name = "sqlite:///test.db"

    ss = StepingStone(L, s, mu12, mu21)

    intdb = InterwithDb(db_name, ss, -1)

    intdb.dumpSetting()

    intdb.dumpConfigeration()

    #for i in range(100):
        #print i
        #ss = StepingStone(L, step, s, mu12, mu21, session, i)
        #ss.simulation()

if __name__ == "__main__":
    import sys
    _, timesofrun = sys.argv
    timesofrun = int(timesofrun)
    #run()
    #step = 10000
    #L = 1000
    #s = 0.
    #mu21 = 0
    #mu12 = 0
    #session = None
    #ss = StepingStone(L, s, mu12, mu21)
    #print getTotalF(ss)
    #ss.getHeterozygosity()
    #getf(timesofrun)
    #a = boundry(0)
    #b = boundry(0.2)
    #a = final()
    final(timesofrun)
