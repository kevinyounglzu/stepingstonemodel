# -*- coding: utf8 -*-
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class Setting(Base):
    """
    Table for basic settings.
    """
    __tablename__ = "settings"
    id = Column(Integer, primary_key=True)
    length = Column(Integer)
    s = Column(Float)
    mu12 = Column(Float)
    mu21 = Column(Float)

    def __repr__(self):
        return "<Setting obj:\nL: %d\ns: %f\nmu12: %f\nmu21: %f>" \
            % (self.length, self.s, self.mu12, self.mu21)


class Data(Base):
    """
    Table for data storage.
    """
    __tablename__ = "data"
    id = Column(Integer, primary_key=True)
    generation = Column(Integer)
    index = Column(Integer)
    allele = Column(Integer)
    timesofrun = Column(Integer)

    def __repr__(self):
        return "<Data obj: step %d index %d allele %d timesofrun %d>" \
            % (self.step, self.index, self.allele, self.timesofrun)


class SqlSession(object):
    def __init__(self, db_name):
        self.engine = create_engine(db_name, echo=False)

    def createASession(self):
        Session = sessionmaker(bind=self.engine)
        return Session()

    def initializeDb(self):
        Base.metadata.create_all(self.engine)
